package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	// Channels used to respond to events
	stopChannel chan os.Signal
	ticker      *time.Ticker

	// Custom logger
	log *Logger

	// HTTP Server
	server *http.Server

	// Latest metrics collected and marshalled
	latestMetrics []byte
)

func init() {
	log = NewLogger()

	parseConfig()
	initClient()

	stopChannel = make(chan os.Signal, 1)
	ticker = time.NewTicker(cfg.RiakStatsDelta)
}

func main() {
	// start
	log.Out("Starting exporter", cfg)
	signal.Notify(stopChannel, os.Interrupt)
	go serve()
	go collect()

	// stop
	<-stopChannel
	log.Out("Shutting down exporter...")
	ticker.Stop()
	server.Shutdown(context.Background())
	context.WithTimeout(context.Background(), 5*time.Second)
	log.Out("Exporter gracefully stopped")
}

func serve() {
	server = &http.Server{Addr: cfg.SelfAddr}

	http.Handle("/me", promhttp.Handler())
	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		w.Write(latestMetrics)
	})

	err := server.ListenAndServe()
	if err != nil {
		log.Fatal("Can not start HTTP server", err)
	}
}

func collect() {
	for range ticker.C {
		if metrics, ok := getMetrics(); ok {
			latestMetrics = metrics.MarshalPrometheus()
		} else {
			latestMetrics = []byte{}
		}
	}
}
