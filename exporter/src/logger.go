package main

import (
	liblog "log"
	"os"
)

// Logger wraps two stdlib's loggers
// Prints each argument on new line
// Implements Config.Quite option
type Logger struct {
	lout *liblog.Logger
	lerr *liblog.Logger
}

// NewLogger creates new Logger instance
// and returns reference to it
func NewLogger() *Logger {
	l := &Logger{}
	l.lout = liblog.New(os.Stdout, "", 0)
	l.lerr = liblog.New(os.Stderr, "", 0)
	return l
}

// Out logs stuff to standart output
func (l Logger) Out(vv ...interface{}) {
	for _, v := range vv {
		l.lout.Println(v)
	}
}

// Err logs stuff to standart error
// if Config.Quite is not set
func (l Logger) Err(vv ...interface{}) {
	if !cfg.Quite {
		for _, v := range vv {
			l.lerr.Println(v)
		}
	}
}

// Fatal prints to stderr and terminates program
func (l Logger) Fatal(vv ...interface{}) {
	for _, v := range vv {
		liblog.Fatalln(v)
	}
}
