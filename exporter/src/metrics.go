package main

import (
	"bytes"
	"fmt"
	"github.com/iancoleman/strcase"
	"io"
	"reflect"
	"text/template"
)

// MetricRecordTemplate contains txt temlate for
// metrics record
const MetricRecordTemplate = `
# HELP {{ .Name }} {{ .Help }}
# TYPE {{ .Name }} gauge
{{ .Name }} {{ .Value }}
`

var (
	// MetricRecord is parsed template instance
	MetricRecord *template.Template
)

func init() {
	var err error
	MetricRecord, err = template.New("record").Parse(MetricRecordTemplate)
	if err != nil {
		log.Fatal("Error parsing MetricRecord", err)
	}
}

// Metrics struct defines metrics object
// being exposed from Riak stats to Prometheus
// Contains CPU and memory related metrics
type Metrics struct {
	SysProcessCount     int `json:"sys_process_count"     help:"Number of processes currently running in the Erlang VM"`
	CPUAvg1             int `json:"cpu_avg1"              help:"Undocumented by Basho"`
	CPUAvg15            int `json:"cpu_avg15"             help:"Undocumented by Basho"`
	CPUAvg5             int `json:"cpu_avg5"              help:"Undocumented by Basho"`
	CPUNprocs           int `json:"cpu_nprocs"            help:"Undocumented by Basho"`
	MemAllocated        int `json:"mem_allocated"         help:"Undocumented by Basho"`
	MemTotal            int `json:"mem_total"             help:"Undocumented by Basho"`
	MemoryAtom          int `json:"memory_atom"           help:"Undocumented by Basho"`
	MemoryAtomUsed      int `json:"memory_atom_used"      help:"Undocumented by Basho"`
	MemoryBinary        int `json:"memory_binary"         help:"Undocumented by Basho"`
	MemoryCode          int `json:"memory_code"           help:"Undocumented by Basho"`
	MemoryEts           int `json:"memory_ets"            help:"Undocumented by Basho"`
	MemoryProcesses     int `json:"memory_processes"      help:"Total amount of memory allocated for Erlang processes (in bytes)"`
	MemoryProcessesUsed int `json:"memory_processes_used" help:"Total amount of memory used by Erlang processes (in bytes)"`
	MemorySystem        int `json:"memory_system"         help:"Undocumented by Basho"`
	MemoryTotal         int `json:"memory_total"          help:"Undocumented by Basho"`
}

// MarshalPrometheus encodes Metrics instance to a byte array
// according Prometheus metrics naming conventions
// https://prometheus.io/docs/practices/naming/
// All the metrics are gauges
func (s Metrics) MarshalPrometheus() []byte {
	var buf bytes.Buffer

	t := reflect.TypeOf(s)

	for i := 0; i < t.NumField(); i++ {
		metric := FromMetricsField(t.Field(i), reflect.ValueOf(s))
		metric.Write(&buf)
	}
	return buf.Bytes()
}

// Metric represents one gauge metric record ready for output
type Metric struct {
	Name  string
	Help  string
	Value interface{}
}

// FromMetricsField exposes Metric from Metrics struct field
func FromMetricsField(f reflect.StructField, v reflect.Value) *Metric {
	return &Metric{
		Name:  strcase.ToSnake(fmt.Sprintf("Riak%v", f.Name)),
		Help:  f.Tag.Get("help"),
		Value: reflect.Indirect(v).FieldByName(f.Name),
	}
}

// WriteTo writes Metric marshalled to given writer
func (m Metric) Write(w io.Writer) {
	err := MetricRecord.Execute(w, m)
	if err != nil {
		log.Err("Error marshalling MetricRecord", err)
	}
}
