package main

import (
	"time"

	"github.com/caarlos0/env"
)

type config struct {
	// Riak coordinator address
	RiakAddr string `env:"RIAK_ADDR,required"`

	// Timeout to wait for riak stats http endpoint
	RiakStatsTimeout time.Duration `env:"RIAK_STATS_TIMEOUT" envDefault:"50ms"`

	// Riak stats pull timelapse
	RiakStatsDelta time.Duration `env:"RIAK_STATS_DELTA" envDefault:"1s"`

	// Self addr
	SelfAddr string `env:"SELF_ADDR,required"`

	// Produce less logs
	Quite bool `env:"QUITE"`
}

var cfg config

func parseConfig() {
	cfg = config{}
	err := env.Parse(&cfg)
	if err != nil {
		log.Fatal(err)
	}
}
