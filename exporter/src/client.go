package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

var client *http.Client

func initClient() {
	client = &http.Client{
		Timeout: cfg.RiakStatsTimeout,
	}
}

func getMetrics() (*Metrics, bool) {
	resp, err := client.Get(statsURL())
	if err != nil {
		log.Err("Error getting riak stats", err)
		return nil, false
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Err("Error reading riak stats.", err)
		return nil, false
	}

	metrics := &Metrics{}
	err = json.Unmarshal(body, metrics)
	if err != nil {
		log.Err("Error decoding riak stats", err)
		return nil, false
	}

	return metrics, true
}

func statsURL() string {
	return fmt.Sprintf("http://%v/stats", cfg.RiakAddr)
}
