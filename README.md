You need Docker and Docker Compose to be installed on your host as well as Docker Machine.

To start the stack:

```
docker-compose up
```

Then checkout next urls:

- http://docker.local:8000 exporter metrics endpoint
- http://docker.local:8001/stats Riak coordinator stats endpoint
- http://docker.local:8002/graph Prometheus UI
- http://docker.local:8003 Grafana

Where `docker.local` should point to your Docker Machine instance.

Use admin/admin to get into Grafana.
